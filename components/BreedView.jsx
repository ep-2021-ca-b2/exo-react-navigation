import React, { useState, useEffect} from "react";
import { Text, ScrollView, Image } from "react-native";


export default function BreedView({ route }) {
	const [images, setImages] = useState([])
	useEffect(() => {
		fetch(`https://dog.ceo/api/breed/${route.params.race}/images/random/10`)
			.then(response => response.json())
			.then(json => setImages(json.message))
	}, [])
	return (
		<ScrollView>
			{images.map(image => (
				<Image
					key={image}
					source={{ uri: image }}
					style={{width: '100%', height: 200}}
					resizeMode="contain"
				/>
			))}
		</ScrollView>
	)
	return <Text>{route.params.race}</Text>
}
